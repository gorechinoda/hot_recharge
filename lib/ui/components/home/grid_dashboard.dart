import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hot_recharge/database_worker/table_names.dart';
import '../../../database_worker/db_helper.dart';
import '../navigation_bloc.dart';
import '../navigation_events.dart';

class GridDashboard extends StatefulWidget {
  GridDashboard({Key key}) : super(key: key);

  @override
  _GridDashboardState createState() => _GridDashboardState();
}

class _GridDashboardState extends State<GridDashboard> {
  Item item1 = new Item(
    title: "Contacts",
    subtitle: "contacts",
    event: "All contacts",
    icon: Icons.contacts,
    count: DBHelper.db.getCount(TabeNames.contacts),
    pageEvent: NavigationEvents.ContactsPageClickedEvent,
  );

  Item item2 = new Item(
    title: "Purchases",
    subtitle: "purchases",
    event: "All Purchase",
    icon: Icons.attach_money,
    count: DBHelper.db.getCount(TabeNames.purchases),
    pageEvent: NavigationEvents.PurchasesPageClickedEvent,
  );

  Item item3 = new Item(
    title: "Sales",
    subtitle: "sales",
    event: "All sales",
    icon: Icons.attach_money,
    count: DBHelper.db.getCount(TabeNames.sales),
    pageEvent: NavigationEvents.SalesPageClickedEvent,
  );

  Item item4 = new Item(
    title: "NetOne",
    subtitle: "netone",
    event: "Netone Contacts",
    icon: Icons.network_cell,
    count: DBHelper.db.countNetworkProviderSales('071'),
    pageEvent: NavigationEvents.NetWorkProviderClickedEvent,
  );

  Item item5 = new Item(
    title: "Telecel",
    subtitle: "telecel",
    event: "Telecel contacts",
    icon: Icons.network_cell,
    count: DBHelper.db.countNetworkProviderSales('073'),
    pageEvent: NavigationEvents.NetWorkProviderClickedEvent,
  );

  Item item6 = new Item(
    title: "Econet",
    subtitle: "econet",
    event: "Econet Contacts",
    icon: Icons.network_cell,
    count: DBHelper.db.countNetworkProviderSales('077'),
    pageEvent: NavigationEvents.NetWorkProviderClickedEvent,
  );

  @override
  Widget build(BuildContext context) {
    List<Item> myList = [item1,item2,item3,item4,item5,item6];
    
    return Flexible(
      child: GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 1.0,
        padding: EdgeInsets.only(
          left: 16,
          right: 16,
        ),
        crossAxisSpacing: 18,
        mainAxisSpacing: 18,
        children: myList.map((data){
          return FutureBuilder<int>(
            future: data.count,

            builder: (BuildContext context, AsyncSnapshot<int> snapshot){
              Widget wow;
              if(snapshot.hasData){
                wow = Text(
                  '${snapshot.data} ${data.subtitle}',
                  style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      color: Colors.white38,
                      fontSize: 10,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                );
              }else if(snapshot.hasError){
                wow = Text(
                  'Error :${snapshot.error}',
                  style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      color: Colors.red,
                      fontSize: 10,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                );
              }else{
                wow = SizedBox(
                  child: CircularProgressIndicator(),
                  width: 10,
                  height: 10,
                );
              }
              return InkWell(
                onTap: () {
                  // Scaffold.of(context).showSnackBar(SnackBar(
                  //   content: Text('Tap'),
                  // ));
                  BlocProvider.of<NavigationBloc>(context).add(data.pageEvent);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(46, 110, 249, 0.9),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // Image.asset(data.img, width: 42,),
                      Icon(
                        data.icon,
                        color: Colors.white,
                        size: 42,
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Text(
                        data.title,
                        style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      wow,

                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        data.event,
                        style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                            color: Colors.white70,
                            fontSize: 11,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        }).toList(),
      ),      
    );
  }
}

class Item{
  String title;
  String event;
  String subtitle;
  IconData icon;
  Future<int> count;
  NavigationEvents pageEvent;
  
  Item({this.event,this.subtitle,this.title,this.icon,this.count,this.pageEvent});
}


