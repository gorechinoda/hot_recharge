import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hot_recharge/database_worker/db_helper.dart';
import 'package:hot_recharge/model/user_details.dart';

import 'navigation_bloc.dart';
import 'navigation_events.dart';

class HotRechargeLayout extends StatefulWidget{
  final Widget body;
  final String page;
  HotRechargeLayout({@required this.body,@required this.page});

  @override
  _HotRechargeLayoutState createState() => _HotRechargeLayoutState();
}

class _HotRechargeLayoutState extends State<HotRechargeLayout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              top: 40,
              bottom: 10,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FutureBuilder<UserDetails>(
                  future: DBHelper.db.getUser(),
                  builder: (context, snapshot) {
                    return snapshot.hasData?  snapshot.data.name.contains("NoUser")? MaterialButton(
                      color: Color.fromRGBO(46, 110, 249, 0.9),
                      splashColor: Color.fromRGBO(46, 110, 249, 0.7),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.edit,
                            color: Colors.white,
                          ),
                          Text(
                            "Register",
                            style: TextStyle(
                              color: Colors.white,
                              fontStyle: FontStyle.italic
                            ),
                          ),
                        ],
                      ),
                      onPressed: (){
                        BlocProvider.of<NavigationBloc>(context).add(NavigationEvents.SettingsPageClickedEvent);
                      }
                    )
                    : Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${snapshot.data.name} ${snapshot.data.surname}",
                          style: GoogleFonts.openSans(
                            textStyle: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Icon(
                              Icons.monetization_on,
                              size: 18,
                              color: Color(0xffa29aac),
                            ),
                            Text(
                              "${snapshot.data.balance}",
                              style: GoogleFonts.openSans(
                                textStyle: TextStyle(
                                  color: Color(0xffa29aac),
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Text(
                          widget.page,
                          style: GoogleFonts.openSans(
                            textStyle: TextStyle(
                              color: Color(0xffa29aac),
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        
                      ],
                    ) : snapshot.hasError?
                    Text("Error") :
                    SizedBox(
                      child: CircularProgressIndicator(),
                      width: 10,
                      height: 10,
                    );
                  }
                ),
                IconButton(
                  alignment: Alignment.topCenter,
                  icon: Icon(
                    Icons.notifications, 
                    size: 24,
                  ),
                  onPressed: (){}
                ),
              ],
            ),
          ),
          widget.body,
        ],
      ),
    );
  }
}