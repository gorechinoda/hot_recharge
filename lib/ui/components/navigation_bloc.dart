import 'package:bloc/bloc.dart';
import 'package:hot_recharge/ui/components/navigation_events.dart';
import 'package:hot_recharge/ui/components/navigation_states.dart';
import 'package:hot_recharge/ui/pages/buy_airtime.dart';
import 'package:hot_recharge/ui/pages/contacts.dart';
import 'package:hot_recharge/ui/pages/home.dart';
import 'package:hot_recharge/ui/pages/network_providers.dart';
import 'package:hot_recharge/ui/pages/purchases.dart';
import 'package:hot_recharge/ui/pages/sales.dart';
import 'package:hot_recharge/ui/pages/settings.dart';

class NavigationBloc extends Bloc<NavigationEvents, NavigationStates>{
  @override
  NavigationStates get initialState => Home();

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvents event) async* {
    switch (event) {
      case NavigationEvents.HomePageClickedEvent:
        yield Home();
        break;
      case NavigationEvents.SettingsPageClickedEvent:
        yield Settings();
        break;
      case NavigationEvents.BuyAirtimePageClickedEvent:
        yield BuyAirtime();
        break;
      case NavigationEvents.ContactsPageClickedEvent:
      yield ContactsPage();
        break;
      case NavigationEvents.PurchasesPageClickedEvent:
      yield PurchasesPage();
        break;
      case NavigationEvents.SalesPageClickedEvent:
        yield SalesPage();
        break;
      case NavigationEvents.NetWorkProviderClickedEvent:
        yield NetWorkProviderPage();
        break;
    }
  }

}