import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hot_recharge/ui/components/navigation_bloc.dart';
import 'package:hot_recharge/ui/components/navigation_states.dart';
import 'package:hot_recharge/ui/components/sidebar.dart';

class SideBarLayout extends StatelessWidget {
  const SideBarLayout({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    

    return Scaffold(
      body: BlocProvider<NavigationBloc>(
        create: (context)=>NavigationBloc(),
        child: Stack(
          children:<Widget>[
            
            BlocBuilder<NavigationBloc, NavigationStates>(
              builder: (context,state){
                return state as Widget;
              }
            ),
            SideBar(),
          ],
        ),
      ),  
    );
  }
}