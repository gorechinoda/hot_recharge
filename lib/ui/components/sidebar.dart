import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hot_recharge/ui/components/navigation_bloc.dart';
import 'package:hot_recharge/ui/components/navigation_events.dart';
import 'package:rxdart/rxdart.dart';

import 'custom_menu_clipper.dart';

class SideBar extends StatefulWidget {
  const SideBar({Key key}) : super(key: key);

  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> with SingleTickerProviderStateMixin<SideBar> {
  AnimationController _animationController;
  final _animationDuration = const Duration(milliseconds:500);

  StreamController<bool> isSidebarOpenStreamController;
  Stream<bool> isSidebarOpenStream;
  StreamSink<bool> isSidebarOpenStreamSink;
  final List<MenuItem> menuItems = [
    MenuItem(name: "Home", event: NavigationEvents.HomePageClickedEvent, icon: Icons.home),
    MenuItem(name: "Buy Airtime", event: NavigationEvents.BuyAirtimePageClickedEvent, icon: Icons.store),
    MenuItem(name: "Sales", event: NavigationEvents.SalesPageClickedEvent, icon: Icons.attach_money),
    MenuItem(name: "Purchases", event: NavigationEvents.PurchasesPageClickedEvent, icon: Icons.attach_money),
    MenuItem(name: "Contacts", event: NavigationEvents.ContactsPageClickedEvent, icon: Icons.contacts),
    MenuItem(name: "Settings", event: NavigationEvents.SettingsPageClickedEvent, icon: Icons.settings),
  ];

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: _animationDuration,
    );
    isSidebarOpenStreamController = PublishSubject<bool>();
    isSidebarOpenStream = isSidebarOpenStreamController.stream;
    isSidebarOpenStreamSink = isSidebarOpenStreamController.sink;
  }

  @override
  void dispose() {
    _animationController.dispose();
    isSidebarOpenStreamController.close();
    isSidebarOpenStreamSink.close();
    super.dispose();
  }

  void iconPressed(){
    final animationStatus = _animationController.status;
    final isAnimationCompleted = animationStatus == AnimationStatus.completed;

    if(isAnimationCompleted){
      isSidebarOpenStreamSink.add(false);
      _animationController.reverse();
    }else{
      isSidebarOpenStreamSink.add(true);
      _animationController.forward();
    }
  }

  void navigate(NavigationEvents event, BuildContext context){
    iconPressed();
    BlocProvider.of<NavigationBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return StreamBuilder<bool>(
      initialData: false,
      stream: isSidebarOpenStream,
      builder: (context, snapshot) {
        return AnimatedPositioned(
          duration: _animationDuration,
          top: 0,
          bottom: 0,
          left: snapshot.data ? 0 : -screenWidth,
          right: snapshot.data ? 0 : screenWidth - 45,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 3),
                  color: Color.fromRGBO(46, 110, 249, 1),
                  child: ListView.builder(
                    itemCount: menuItems.length + 1,
                    itemBuilder: (context,position){
                      return position==0? SizedBox(
                        height: 100,
                      ) 
                      : 
                      Card(
                        elevation: 3,
                        color: Color.fromRGBO(46, 110, 249, 1),
                          child: ListTile(
                          leading: Icon(
                            menuItems[position-1].icon,
                            color: Colors.white,
                            size: 30,
                          ),
                          title: Text(
                            menuItems[position-1].name,
                            style: TextStyle(
                              color: Colors.white,
                              // fontWeight: FontWeight.bold,
                              fontSize: 25
                            ),
                          ),
                          trailing: Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.white,
                            size: 20,
                          ),
                          onTap: ()=>navigate(menuItems[position-1].event, context),
                        ),
                      );
                    },
                  ),
                  // ListView(
                  //   children: <Widget>[
                  //     SizedBox(
                  //       height: 100,
                  //     ),
                  //     ListTile(
                  //       title: Text('Buy Airtime'),
                  //       onTap: ()=>navigate(NavigationEvents.BuyAirtimePageClickedEvent, context),
                  //     ),
                  //     ListTile(
                  //       title: Text('Settings'),
                  //       onTap: ()=>navigate(NavigationEvents.SettingsPageClickedEvent, context),
                  //     )
                  //   ]
                  // ),
                ),
              ),
              Align(
                alignment: Alignment(0,-0.9),
                child: GestureDetector(
                  onTap: iconPressed,
                  child: ClipPath(
                    clipper: CustomMenuClipPath(),
                    child: Container(
                      height: 110,
                      width: 35,
                      color: Color.fromRGBO(46, 110, 249, 1),
                      alignment: Alignment.centerLeft,
                      child: AnimatedIcon(
                        icon: AnimatedIcons.menu_arrow,
                        progress: _animationController.view,
                        color: Color(0xFF1BB5FD),
                        size: 25,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }
    );
  }
}

class MenuItem{
  String name;
  IconData icon;
  NavigationEvents event;

  MenuItem({this.name,this.icon,this.event});
}