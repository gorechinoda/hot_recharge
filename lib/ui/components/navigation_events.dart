enum NavigationEvents {
  HomePageClickedEvent,
  SettingsPageClickedEvent,
  BuyAirtimePageClickedEvent,
  ContactsPageClickedEvent,
  PurchasesPageClickedEvent,
  SalesPageClickedEvent,
  NetWorkProviderClickedEvent,
}