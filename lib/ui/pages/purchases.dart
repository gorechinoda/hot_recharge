import 'package:flutter/material.dart';
import 'package:hot_recharge/ui/components/layout.dart';
import 'package:hot_recharge/ui/components/navigation_states.dart';

class PurchasesPage extends StatelessWidget with NavigationStates{
 
  @override
  Widget build(BuildContext context) {
    return HotRechargeLayout(
      page: "Purchases",
      body: Text("No purchases"),
    );
  }
}