import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hot_recharge/model/contacts.dart';
import 'package:hot_recharge/ui/components/layout.dart';
import 'package:hot_recharge/ui/components/navigation_states.dart';
import 'package:hot_recharge/ui/pages/contacts.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BuyAirtime extends StatefulWidget with NavigationStates{
  
  @override
  _BuyAirtimeState createState() => _BuyAirtimeState();
}

class _BuyAirtimeState extends State<BuyAirtime> {

  GlobalKey<FormBuilderState> _formBuilderKey = GlobalKey<FormBuilderState>();
  GlobalKey<FormBuilderState> _alertFormBuilderKey = GlobalKey<FormBuilderState>();
  TextEditingController phoneNumberController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return HotRechargeLayout(
      page: "Buy Airtime",
      body: Flexible(
        child: Card(
          elevation: 8,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25)
          ),
          child: FormBuilder(
            key: _formBuilderKey,
            initialValue: {
              "phoneNumber": "123456"
            },
            child: ListView(
              // crossAxisCount: 2,
              // childAspectRatio: 1.0,
              shrinkWrap: true,
              // mainAxisSpacing: 1,
              // crossAxisSpacing: 10,
              padding: EdgeInsets.only(
                left: 16,
                right: 16,
                top: 10,
                bottom: 10
              ),
              // crossAxisSpacing: 18,
              // mainAxisSpacing: 18,
              children:<Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    top: 18,
                    bottom: 18,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text(
                        "To choose From Existing Contacts press ",
                        style: TextStyle(
                          color: Color.fromRGBO(46, 110, 249, 0.9),
                        ),
                      ),
                      FaIcon(
                        FontAwesomeIcons.addressBook,
                        color: Color.fromRGBO(46, 110, 249, 0.9),
                      )
                    ],
                  ),
                ),
                FormBuilderTextField(
                  attribute: "phoneNumber",
                  controller: phoneNumberController,
                  decoration: InputDecoration(
                      prefixIcon: Icon(FontAwesomeIcons.phone),
                      labelText: "Enter phone number",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      suffix: GestureDetector(
                        child: FaIcon(
                          FontAwesomeIcons.addressBook,
                          color: Color.fromRGBO(46, 110, 249, 0.9),
                        ),
                        onTap: (){
                          var e;
                          e =showBottomSheet(context: context, builder: (data){
                              return Container(
                                height: 300,
                                child: Card(
                                  color: Color.fromRGBO(46, 110, 249, 0.9),
                                   shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(50),
                                        topRight: Radius.circular(50),
                                      ),
                                    ),
                                  elevation: 3,
                                  child: ContactsView(
                                    onTap: (Contacts data){
                                      // print(data.surname);
                                      print(_formBuilderKey.currentState.value["phoneNumber"]);
                                      // setState(() {
                                      //   _formBuilderKey.currentState.setAttributeValue("phoneNumber", "value");
                                      // });
                                      phoneNumberController.text = "${data.number}";
                                      // _formBuilderKey.currentState.setAttributeValue("phoneNumber", data.number);
                                      // print(_formBuilderKey.currentState.value["phoneNumber"]);
                                      // _formBuilderKey.currentState.save();
                                      e.close();
                                    },
                                  ),
                                )
                              );
                            },
                            // backgroundColor: Colors.red,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(55),
                                topRight: Radius.circular(55),
                              ),
                            ),
                          );
                          
                          //e.close();
                          // AlertDialog(
                          //   title: Text("Ndyp"),
                          //   content: Container(
                          //     height: 200,
                          //     child: ContactsView()
                          //   ),
                          // );
                        },
                      ),
                  ),
                  validators: [
                    FormBuilderValidators.required(),
                    FormBuilderValidators.minLength(9,errorText: 'Please enter valid phone number'),
                    FormBuilderValidators.maxLength(11, errorText: 'Please enter valid phone number'),
                    // (value){
                    //   if(value.isEmpty){
                    //     return 'Enter phone number';
                    //   }else if(value.length!=10){
                    //     return ;
                    //   }
                    //   return null;
                    // }
                  ],
                  keyboardType: TextInputType.number,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 40,
                  ),
                  child: FormBuilderTextField(
                    attribute: "amount",
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.attach_money),
                      labelText: "Enter Amount",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    validators: [
                      FormBuilderValidators.required(errorText: 'Enter some amount'),
                    ],
                    // validator: (value){
                    //   if(value.isEmpty){
                    //     return ;
                    //   }
                    //   return null;
                    // },
                    keyboardType: TextInputType.number,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 30,
                    left: 50,
                    right: 50,
                  ),
                  child: MaterialButton(
                      color: Color.fromRGBO(46, 110, 249, 0.9),
                      splashColor: Color.fromRGBO(46, 110, 249, 0.7),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: Text(
                        'Submit',
                        style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      height: 40,
                      elevation: 10,
                      highlightElevation: 8,
                      onPressed: (){
                        if(_formBuilderKey.currentState.validate()){
                          alert(context);
                        }
                      },
                      
                    ),
                ),
              ]
            )
          ),
        ),
      ),
    );
  }


  alert(BuildContext context){
    // var bottomSheet = showBottomSheet(context: context, builder: (data){
    //   return FormBuilder(
    //     key: _alertFormBuilderKey,
    //     initialValue: {
    //       "number":"0",
    //     },
    //     child: Column(
    //       children: <Widget>[
    //         FormBuilderTextField(
    //           attribute: "name",
    //           decoration: InputDecoration(
    //             icon: Icon(Icons.account_circle),
    //             labelText: 'Name',
    //           ),
    //         ),
    //          FormBuilderTextField(
    //           attribute: "surname",
    //           decoration: InputDecoration(
    //             icon: Icon(Icons.account_circle),
    //             labelText: 'Surname',
    //           ),
    //         ),
    //         FormBuilderTextField(
    //           attribute: "number",
    //           // obscureText: true,
    //           decoration: InputDecoration(
    //             icon: Icon(Icons.lock),
    //             labelText: 'number',
    //           ),
    //           readOnly: true,
    //         ),
    //       ],
    //     ),
    //   );
    // });
    Alert(
      context: context,
      title: "Add Contact",
      content: FormBuilder(
        key: _alertFormBuilderKey,
        initialValue: {
          "number": phoneNumberController.text,
        },
        child: Column(
          children: <Widget>[
            FormBuilderTextField(
              attribute: "name",
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.account_circle),
                labelText: 'Name',
              ),
            ),
             FormBuilderTextField(
              attribute: "surname",
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.account_circle),
                labelText: 'Surname',
              ),
            ),
            FormBuilderTextField(
              attribute: "number",
              // obscureText: true,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.lock),
                labelText: 'Phone number',
              ),
              readOnly: true,
            ),
          ],
        ),
      ),
      buttons: [
        DialogButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            "Save",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )
      ]
    ).show();
  
  }
}