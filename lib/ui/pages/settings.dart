import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:hot_recharge/model/user_details.dart';
import 'package:hot_recharge/ui/components/layout.dart';
import 'package:hot_recharge/ui/components/navigation_states.dart';
import 'package:sim_service/models/sim_data.dart';
import 'package:sim_service/sim_service.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hot_recharge/database_worker/db_helper.dart';

class Settings extends StatefulWidget with NavigationStates {
  const Settings({Key key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  SimData _simData;
  bool gettingData = false;
  bool editSettings = false;
  UserDetails _userDetails;
  String _cardDesplayName;
  int id;
  GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  
  String simCard = "";

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    SimData simData;

    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      setState(() {
        gettingData = true; 
      });
      simData = await SimService.getSimData;
    } on Exception {
      print(simData);
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance. 
    if (!mounted) return;

    setState(() {
      gettingData = false;
      _simData = simData;
    });
  }

  void saveData(){
    if(!editSettings){
      setState(() {
        editSettings = true;
      });
      return;
    }
    
                      
    if (_fbKey.currentState.saveAndValidate()) {
      if(_cardDesplayName==null||_cardDesplayName.isEmpty){
        _cardDesplayName = _simData.cards[0].displayName;
      }
      _userDetails = new UserDetails(
        rowid: _fbKey.currentState.value['rowid'],
        card: _cardDesplayName,
        name: _fbKey.currentState.value['name'],
        surname: _fbKey.currentState.value['surname'],
        nationalID: _fbKey.currentState.value['nationalID'],
        email: _fbKey.currentState.value['email'],
        pin: int.parse(_fbKey.currentState.value['pin']),
        balance: _fbKey.currentState.value['balance']!=null? _fbKey.currentState.value['balance']: 0.0,
      );
      
      print(_userDetails.toMap().toString());
      
      if(_userDetails.rowid==null){
        DBHelper.db.newRecord(_userDetails).then((onValue){
          print(onValue);
        });
      }else{
        DBHelper.db.updateRecord(_userDetails);
      }
       setState(() {
        editSettings = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return HotRechargeLayout(
      page: "Settings",
      body: Flexible(
        child: Padding(
          padding: const EdgeInsets.only(
            right: 5,
            left: 15,
            bottom: 5,
          ),
          child: Card(
            elevation: 8,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: FutureBuilder<UserDetails>(
                future: DBHelper.db.getUser(),
                builder: (context, snapshot) {
                  print("Hello Milton");
                  Widget dataToreturn = SizedBox(
                    child: CircularProgressIndicator(),
                    width: 50,
                    height: 50,
                  );
                  if(snapshot.hasData){
                    _userDetails = snapshot.data;
                    if(snapshot.data.name.contains("NoUser")){
                      _userDetails.name = "";
                    }

                    
                    print(snapshot.data.card);
                    dataToreturn = FormBuilder(
                      readOnly: !editSettings,
                      initialValue: {
                        //Initialize data in here
                        'name': _userDetails.name,
                        'surname': _userDetails.surname,
                        'nationalID': _userDetails.nationalID,
                        'email': _userDetails.email,
                        'pin': "${_userDetails.pin}",
                        'balance': _userDetails.balance,
                        'rowid': _userDetails.rowid,
                      },
                      key: _fbKey,
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 15,
                          right: 10,
                        ),
                        child: ListView(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                MaterialButton(
                                  onPressed: saveData,
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          right: 10
                                        ),
                                        child: Icon(
                                          editSettings? FontAwesomeIcons.save: FontAwesomeIcons.edit,
                                          color: Color.fromRGBO(46, 110, 249, 0.9),
                                        ),
                                      ),
                                      Text(
                                        editSettings? "save": "edit",
                                        style: TextStyle(
                                          color: Color.fromRGBO(46, 110, 249, 0.9),
                                          fontSize: 20,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Card(
                              elevation: 4,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                )
                              ),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Select Sim card",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w900,
                                      fontSize: 16,
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: _simData != null && _simData.cards is List && _simData.cards.length > 0 ? _simData.cards.map((SimCard card) {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Radio(
                                            value: card.displayName,
                                            groupValue: snapshot.data.card==null? _cardDesplayName : snapshot.data.card,
                                            onChanged: (data){
                                              setState(() {
                                                _cardDesplayName = data;
                                              });
                                            },
                                          ),
                                          Column(
                                            children: <Widget>[
                                              Text(
                                                card.displayName.toString(),
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              Text(
                                                card.carrierName.toString(),
                                                style: TextStyle(
                                                  fontStyle: FontStyle.italic,
                                                  color: Color.fromRGBO(107, 104, 104, 0.5),
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      );
                                    }).toList():
                                    
                                    gettingData ? <Widget>[
                                      Center(
                                        child: CircularProgressIndicator(),
                                      )
                                    ] : <Widget>[
                                      Center(
                                        child: Text('faild to laod data'),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            FormBuilderTextField(
                              attribute: "name",
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                labelText: "Name",
                                prefixIcon: Icon(FontAwesomeIcons.userEdit),
                              ),
                              validators: [
                                FormBuilderValidators.required(
                                  errorText: "Please type in your name"
                                )
                              ],
                            ),
                            FormBuilderTextField(
                              attribute: "surname",
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                labelText: "Surname",
                                prefixIcon: Icon(FontAwesomeIcons.userEdit),
                              ),
                              validators: [
                                FormBuilderValidators.required(
                                  errorText: "Please type in your surname"
                                )
                              ],
                            ),
                            FormBuilderTextField(
                              attribute: "nationalID",
                              decoration: InputDecoration(
                                labelText: "National ID",
                                prefixIcon: Icon(
                                  FontAwesomeIcons.idBadge
                                ),
                              ),
                              validators: [
                                FormBuilderValidators.required(
                                  errorText: "Please type in your national ID"
                                ),
                                FormBuilderValidators.minLength(7),
                                // FormBuilderValidators.maxLength(10),
                              ],
                            ),

                            FormBuilderTextField(
                              attribute: "email",
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                labelText: "Email",
                                prefixIcon: Icon(FontAwesomeIcons.envelope),
                              ),
                              validators: [
                                FormBuilderValidators.required(
                                  errorText: "Please type in your Email"
                                ),
                                FormBuilderValidators.email(
                                  errorText: "invalid email e.g (mgore@gmail.com)"
                                )
                              ],
                            ),
                            FormBuilderTextField(
                              attribute: "pin",
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                labelText: "Pin",
                                prefixIcon: Icon(FontAwesomeIcons.key),
                              ),
                              validators: [
                                FormBuilderValidators.maxLength(4),
                                FormBuilderValidators.minLength(4),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 25,
                              ),
                              child: editSettings? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  MaterialButton(
                                    color: Color.fromRGBO(46, 110, 249, 0.9),
                                    splashColor: Color.fromRGBO(46, 110, 249, 0.7),
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            right: 10,
                                          ),
                                          child: Icon(
                                            FontAwesomeIcons.save,
                                            color: Colors.white,
                                          ),
                                        ),
                                        Text(
                                          "Save",
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white
                                          ),
                                        ),
                                      ],
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                    ),
                                    onPressed: saveData,
                                  ),
                                ],
                              ) : Container(),
                            )
                          ],
                        ),
                      ),
                    );



                  }else if(snapshot.hasError){
                    dataToreturn = Center(
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.error),
                          Text("Error connecting to DB"),
                        ]
                        
                      ),
                    );
                  }

                  return dataToreturn;
                }
              ),
            ),
          ),
        ),
      )

    );
  }
}

