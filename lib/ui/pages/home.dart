import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
import 'package:hot_recharge/ui/components/home/grid_dashboard.dart';
import 'package:hot_recharge/ui/components/layout.dart';
import 'package:hot_recharge/ui/components/navigation_states.dart';

class Home extends StatelessWidget with NavigationStates{
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HotRechargeLayout(
      page: "Home",
      body: GridDashboard(),
    );
    // Scaffold(
    //   backgroundColor: Colors.white,
    //   body: Column(
    //     children: <Widget>[
    //       SizedBox(
    //         height: 50,
    //       ),
    //       Padding(
    //         padding: EdgeInsets.only(
    //           left: 16,
    //           right: 16,
    //         ),
    //         child: Row(
    //           mainAxisAlignment: MainAxisAlignment.end,
    //           children: <Widget>[
    //             Column(
    //               mainAxisAlignment: MainAxisAlignment.start,
    //               children: <Widget>[
    //                 Text(
    //                   "Milton Gore",
    //                   style: GoogleFonts.openSans(
    //                     textStyle: TextStyle(
    //                       color: Colors.black,
    //                       fontSize: 18,
    //                       fontWeight: FontWeight.bold,
    //                     ),
    //                   ),
    //                 ),
    //                 SizedBox(
    //                   height: 4,
    //                 ),
    //                 Text(
    //                   "Home",
    //                   style: GoogleFonts.openSans(
    //                     textStyle: TextStyle(
    //                       color: Color(0xffa29aac),
    //                       fontSize: 18,
    //                       fontWeight: FontWeight.w600,
    //                     ),
    //                   ),
    //                 ),
    //               ],
    //             ),
    //             IconButton(
    //               alignment: Alignment.topCenter,
    //               icon: Icon(
    //                 Icons.notifications, 
    //                 size: 24,
    //               ),
    //               onPressed: (){}
    //             )
    //           ],
    //         ),
    //       ),
    //       SizedBox(
    //         height: 40,
    //       ),
    //       GridDashboard(),
    //     ],
    //   ),
    // );
  }
}