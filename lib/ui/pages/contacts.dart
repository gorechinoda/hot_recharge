import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hot_recharge/database_worker/table_names.dart';
import 'package:hot_recharge/model/contacts.dart';
import 'package:hot_recharge/ui/components/layout.dart';
import 'package:hot_recharge/ui/components/navigation_states.dart';
import 'package:hot_recharge/database_worker/db_helper.dart';

class ContactsPage extends StatelessWidget with NavigationStates{
  const ContactsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HotRechargeLayout(
      page: "Contacts",
      body: Flexible(child: ContactsView()),
    );
  }
}

class ContactsView extends StatefulWidget{

  final Function onTap;

  ContactsView({this.onTap});


  @override
  _ContactsViewState createState() => _ContactsViewState();
}

class _ContactsViewState extends State<ContactsView> {

  List<Contacts>  myContacts = new List();
  List<Contacts>  myFilteredContacts = new List();  
  
  @override
  void initState() { 
    super.initState();
    DBHelper.db.getAll(TabeNames.contacts).then((value){
      setState(() {
        myContacts = value.map((e) => Contacts.fromMap(e)).toList();
        myFilteredContacts = value.map((e) => Contacts.fromMap(e)).toList();
        myContacts = data;
        myFilteredContacts = data;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(
            right: 25,
            left: 30,
          ),
          child: Card(
            elevation: 4,
            child: TextField(
              decoration: InputDecoration(
                labelText: "Search",
                prefixIcon: Icon(
                  FontAwesomeIcons.search,
                  size: 18,
                ),
                
              ),
              style: TextStyle(
                fontSize: 18,
              ),
              onChanged: (value){
                setState(() {  
                  myFilteredContacts = myContacts.where(
                    (element) => (element.name.toLowerCase().contains(value.toLowerCase())) ||
                    (element.surname.toLowerCase().contains(value.toLowerCase())) || 
                    ("${element.number}".contains(value))
                  ).toList();
                });
              },
            ),
          ),
        ),
        Expanded(
          child: myFilteredContacts.length==0? 
          Text(
            "0 contacts",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
              fontStyle: FontStyle.italic
            ),
          )
          :
          ListView.builder(
            padding: EdgeInsets.all(10.0),
            itemCount: myFilteredContacts.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: (){
                  print(myFilteredContacts[index].name);
                  widget.onTap(myFilteredContacts[index]);
                },
                child: Card(
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 30,
                      bottom: 15,
                      top: 10,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              myFilteredContacts[index].name,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              myFilteredContacts[index].surname,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        ),
                        Text(
                          "${myFilteredContacts[index].number}",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.grey,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
          ),
        ),
      ],
    );
  }
}

List<Contacts> data = [
  new Contacts(name: "Milton", number: 0718718958, surname: "Gore"),
  new Contacts(name: "Theo", number: 0718718958, surname: "Bag"),
  new Contacts(name: "Clar", number: 0718718958, surname: "Chingwa"),
  new Contacts(name: "Wow", number: 0718718958, surname: "Woooow"),
  new Contacts(name: "Timukudza", number: 0718718958, surname: "Gore"),
  new Contacts(name: "Tinshe", number: 0718718958, surname: "Murombo"),
  new Contacts(name: "Iwe", number: 0718718958, surname: "Hesi"),
  new Contacts(name: "Hoo", number: 0718718958, surname: "Yeah"),
  new Contacts(name: "Lodza", number: 0718718958, surname: "Simango"),
  new Contacts(name: "Midza", number: 0774226683, surname: "Ndyp"),
  new Contacts(name: "Milton", number: 0718718925, surname: "Gore"),
];
