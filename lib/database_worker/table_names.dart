class TabeNames{
  static final String contacts = 'contacts';
  static final String purchases = 'purchases';
  static final String sales = 'sales';
  static final String networkProviders = 'networkProviders';
  static final String userDetails = 'userDetails';
}