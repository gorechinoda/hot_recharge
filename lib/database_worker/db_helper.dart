import 'dart:ffi';

import 'package:hot_recharge/database_worker/database_workable.dart';
import 'package:hot_recharge/database_worker/table_names.dart';
import 'package:hot_recharge/model/user_details.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';

class DBHelper{
  DBHelper._();

  static final DBHelper db = DBHelper._();

  static Database _database;

  Future<Database> get database async{
    if(_database != null){
      return _database;
    }else{
      _database = await initDB();
      return _database;
    }
  }

  initDB() async{
    return await openDatabase(
      join(await getDatabasesPath(),'hot_recharge.db'),
      onCreate: (db,version) async {
        List<String> wow=[
          'CREATE TABLE ${TabeNames.contacts}(name TEXT, number TEXT UNIQUE, surname TEXT, networkProviderId INTEGER, dateCreated TIMESTAMP NOT NULL)',
          'CREATE TABLE ${TabeNames.userDetails}(name TEXT, surname TEXT, nationalID TEXT, pin INTEGER, balance DECIMAL, email TEXT, card TEXT, dateCreated TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, dateUpdated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
          'CREATE TABLE ${TabeNames.networkProviders}(startCode TEXT, name TEXT, imageUrl TEXT, dateCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
          'CREATE TABLE ${TabeNames.sales}(amount INTEGER, number TEXT, profit DECIMAL, dateSold TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
          'CREATE TABLE ${TabeNames.purchases}(amount INTEGER, datePurchased TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)'
        ];
        wow.forEach((String midza) async=>await db.execute(midza));
      },
      version: 1
    );
  }

  Future<dynamic> newRecord(DatabaseWorkable data) async{
    final db = await database;
    var res = await db.insert(data.getTableName(), data.toMap());
    return res;
  }

  Future<dynamic> updateRecord(DatabaseWorkable data) async{
    final db = await database;
    var res = await db.update(data.getTableName(), data.toMap());
    return res;
  }

  Future<List<Map<String,dynamic>>> getAll(String tableName) async{
    final db = await database;
    var res = await db.query(tableName);
    return res;
  }

  Future<int> getCount(String tableName) async{
    final db = await database;
    var res = await db.query(tableName);
    return res.length;
  }

  Future<int> countNetworkProviderSales(String startCode) async{
    final db = await database;
    var res = await db.rawQuery('select * from ${TabeNames.sales} where number like ?',['%$startCode']);
    return res.length;
  }

  Future<Double> getBalance() async{
    final db = await database;
    var res = await db.query(TabeNames.userDetails);
    return res.first['balance'];
  }

  Future<UserDetails> getUser() async{
    final db = await database;
    var res = await db.query(TabeNames.userDetails);
    return res.isEmpty? new UserDetails(name: "NoUser") : UserDetails.fromMap(res.first);
  }
}