import 'package:hot_recharge/database_worker/database_workable.dart';

class UserDetails implements DatabaseWorkable{
  int rowid;
  String name;
  String surname;
  String nationalID;
  String email;
  int pin;
  double balance;
  String card;
  DateTime dateCreated;

  UserDetails({this.rowid,this.name,this.surname,this.nationalID,this.email,this.pin,this.balance,this.card,this.dateCreated});

  factory UserDetails.fromMap(Map<String,dynamic> json) => new UserDetails(
    rowid: json['rowid'],
    name: json['name'],
    surname: json['surname'],
    nationalID: json['nationalID'],
    email: json['email'],
    pin: json['pin'],
    balance: json['balance']==null? 0.0: double.parse("${json['balance']}"),
    card: json['card'],
    dateCreated: json['dateCreated'],
  );

  Map<String,dynamic> toMap() => {
    'rowid': rowid,
    'name': name,
    'surname': surname,
    'nationalID': nationalID,
    'email': email,
    'pin': pin,
    'balance': balance,
    'card':card,
    'dateCreated': dateCreated,
  };

  @override
  String getTableName() {
    return 'userDetails';
  }
}