import 'package:hot_recharge/database_worker/database_workable.dart';

class NetworkProvider implements DatabaseWorkable{
  int rowid;
  int startCode;
  String name;
  String imageUrl;
  DateTime dateCreated;

  NetworkProvider({this.rowid,this.startCode,this.name,this.imageUrl,this.dateCreated});

  factory NetworkProvider.fromMap(Map<String,dynamic> json) => new NetworkProvider(
    rowid: json['rowid'],
    startCode: json['startCode'],
    name: json['name'],
    imageUrl: json['imageUrl'],
    dateCreated: json['dateCreated'],
  );

 Map<String, dynamic> toMap() => {
    // "rowid": rowid,
    "startCode": startCode,
    "name": name,
    "imageUrl": imageUrl,
    "dateCreated": dateCreated,
  };

  @override
  String getTableName() {
    return 'networkProviders';
  }
}