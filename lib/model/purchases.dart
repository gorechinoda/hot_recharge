import 'package:hot_recharge/database_worker/database_workable.dart';

class Purchases implements DatabaseWorkable{
  int rowid;
  double amount;
  DateTime datePurchased;

  Purchases({this.rowid,this.amount,this.datePurchased});

  factory Purchases.fromMap(Map<String,dynamic> json)=> new Purchases(
    rowid: json['rowid'],
    amount: json['amount'],
    datePurchased: json['datePurchased'],
  );

  Map<String,dynamic> toMap() => {
    // 'rowid': rowid,
    'amount': amount,
    'datePurchased': datePurchased,
  };

  @override
  String getTableName() {
    return 'purchases';
  }
}