import 'package:hot_recharge/database_worker/database_workable.dart';

class Sales implements DatabaseWorkable{
  int rowid;
  double amount;
  int number;
  double profit;
  DateTime dateSold;

  Sales({this.rowid,this.amount,this.number,this.profit,this.dateSold});

  factory Sales.fromMap(Map<String,dynamic> json) => new Sales(
    rowid: json['rowid'],
    amount: json['amount'],
    number: json['number'],
    profit: json['profit'],
    dateSold: json['dateSold'],
  );

  Map<String,dynamic> toMap() => {
    // 'rowid': rowid,
    'anount': amount,
    'number': number,
    'profit': profit,
    'dateSold': dateSold,
  };

  @override
  String getTableName() {
    return 'sales';
  }
}