import 'package:hot_recharge/database_worker/database_workable.dart';
import 'package:hot_recharge/model/network_provider.dart';

class Contacts implements DatabaseWorkable{
  int rowid;
  int number;
  String name;
  String surname;
  NetworkProvider networkProvider;
  DateTime dateCreated;

  Contacts({this.rowid,this.number,this.name,this.surname,this.networkProvider,this.dateCreated});

  factory Contacts.fromMap(Map<String, dynamic> json) => new Contacts(
    rowid: json["rowid"],
    number: json["number"],
    name: json["name"],
    surname: json["surname"],
    networkProvider: new NetworkProvider(rowid: json['networkProviderId']),
    dateCreated: json['dateCreated'],
  );

  Map<String, dynamic> toMap() => {
    // "rowid": rowid,
    "number": number,
    "name": name,
    "surname": surname,
    "networkProviderId": networkProvider.rowid,
    "dateCreated": dateCreated,
  };

  @override
  String getTableName() {
    return 'contacts';
  }
}